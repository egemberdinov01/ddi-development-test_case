from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='all_quiz'),
    path('quiz/create/', views.create, name='create_quiz'),
    path('quiz/<int:quiz_id>/', views.read, name='read_quiz'),
    path('quiz/<int:quiz_id>/passing_quiz/', views.passing_quiz, name='passing_quiz'),
    path('quiz/<int:quiz_id>/add_comment', views.add_comment, name='add_comment'),
]
