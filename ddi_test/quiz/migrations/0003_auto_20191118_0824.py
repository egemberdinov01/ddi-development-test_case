# Generated by Django 2.2.7 on 2019-11-18 08:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0002_auto_20191117_1325'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testresult',
            name='correct_answer_count',
            field=models.IntegerField(verbose_name='Кол-во правильных ответов'),
        ),
        migrations.AlterField(
            model_name='testresult',
            name='questions_count',
            field=models.IntegerField(verbose_name='Кол-во вопросов'),
        ),
    ]
