from django.shortcuts import render, redirect
from django.http.request import HttpRequest
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from .models import Quiz, TestResult, Choice, Comment
from .forms.forms_creating_quiz import QuizForm, QuestionFormset
from .forms.forms_passing_quiz import QuizPassingForm
from .forms.comment_form import CommentForm


def index(request: HttpRequest):
    sort_by_date_types = {
        'desk': '-pub_date',
        '': 'pub_date'
    }
    search_query = request.GET.get('search_query', '')
    not_show_completed = request.GET.get('not_show_completed', '')
    sort_by_date_type = sort_by_date_types[request.GET.get('sort_by_date_type', '')]

    if not_show_completed:
        all_quiz = Quiz.objects.exclude(testresult__user_profile=request.user.profile)
    else:
        all_quiz = Quiz.objects.all()

    if search_query:
        all_quiz = all_quiz.filter(title__icontains=search_query).order_by(sort_by_date_type)
    else:
        all_quiz = all_quiz.all().order_by(sort_by_date_type)

    return render(request, 'quiz/all_quiz.html', {'all_quiz': all_quiz})


@login_required
def create(request: HttpRequest):
    quiz = Quiz()
    if request.method == 'POST':
        quiz_form = QuizForm(request.POST, instance=quiz)
        question_formset = QuestionFormset(request.POST, prefix='questions', instance=quiz)

        if quiz_form.is_valid() and question_formset.is_valid():
            new_quiz = quiz_form.save(commit=False)
            new_quiz.author_profile = request.user.profile
            new_quiz.save()
            question_formset = QuestionFormset(request.POST, prefix='questions', instance=new_quiz)
            question_formset.is_valid()
            question_formset.save()
            return redirect('all_quiz')
    else:
        quiz_form = QuizForm(instance=quiz)
        question_formset = QuestionFormset(instance=quiz, prefix='questions')

    context = {
        'quiz_form': quiz_form,
        'question_formset': question_formset,
    }

    return render(request, 'quiz/create_quiz.html', context)


@login_required
def passing_quiz(request: HttpRequest, quiz_id: int):
    quiz = get_object_or_404(Quiz, pk=quiz_id)
    if request.method == 'POST':

        answers = request.POST.dict()
        questions = quiz.questions.all()
        correct_answer_count = 0
        questions_count = len(questions)
        for question in questions:
            answer = answers[f'{question.id}']
            choice = Choice.objects.get(pk=answer)
            if choice.correct == 1:
                correct_answer_count += 1

        result_test = TestResult(quiz=quiz,
                                 user_profile=request.user.profile,
                                 questions_count=questions_count,
                                 correct_answer_count=correct_answer_count)
        result_test.save()
        quiz.count_passes += 1
        quiz.save()
        return redirect('read_quiz', quiz_id)

    passing_quiz_form = QuizPassingForm(quiz)
    context = {
        'quiz': quiz,
        'passing_quiz_form': passing_quiz_form,
    }
    return render(request, 'quiz/get_tested.html', context)


def read(request: HttpRequest, quiz_id: int):
    quiz = get_object_or_404(Quiz, pk=quiz_id)
    comments = quiz.comments.all().order_by('pub_date')

    if request.user.is_authenticated:
        comment_form = CommentForm()
    else:
        comment_form = None
    context = {
        'comments': comments,
        'quiz': quiz,
        'comment_form': comment_form
    }
    return render(request, 'quiz/quiz_page.html', context)


@login_required
def add_comment(request: HttpRequest, quiz_id: int):
    if request.method == 'POST':
        comment = Comment()
        quiz = get_object_or_404(Quiz, pk=quiz_id)
        comment_form = CommentForm(request.POST, instance=comment)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.author_profile = request.user.profile
            comment.quiz = quiz
            comment.save()
    return redirect('read_quiz', quiz_id)
