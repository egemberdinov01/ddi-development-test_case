from django import template
from ..models import TestResult
from django.core.exceptions import ObjectDoesNotExist

register = template.Library()


@register.filter('get_results_dict')
def get_results_dict(var, profile_id):
    try:
        result = TestResult.objects.get(user_profile_id=profile_id, quiz_id=var.id)
        percent = int(100 / result.questions_count * result.correct_answer_count)
        results = {
            'correct_answer_count': result.correct_answer_count,
            'questions_count': result.questions_count,
            'percent': percent
        }

    except ObjectDoesNotExist:
        results = ''

    return results
