from django.forms import BaseInlineFormSet
from django.forms import ValidationError
from django.forms import inlineformset_factory
from django.forms import ModelForm
from ..models import Quiz, Question, Choice


class QuizForm(ModelForm):
    class Meta:
        model = Quiz
        fields = ('title', 'about')


class BaseQuestionChoiceFormset(BaseInlineFormSet):

    def clean(self):
        super(BaseQuestionChoiceFormset, self).clean()
        count_correct = 0
        validation_errors = []
        for form in self.forms:
            if form.is_bound:
                if form.cleaned_data.get('correct'):
                    count_correct += 1
                    if count_correct > 1:
                        validation_errors.append(ValidationError("Только один ответ может быть правильным!"))
                if not form.cleaned_data.get('choice'):
                    validation_errors.append(ValidationError("Каждый вопрос должен содержать 4 ответа!"))
        if count_correct == 0:
            validation_errors.append(ValidationError("Один из ответов обязательно должен быть верным!"))
        if len(validation_errors) != 0:
            raise ValidationError(validation_errors)


QuestionChoiceFormset = inlineformset_factory(
    parent_model=Question,
    model=Choice,
    fields='__all__',
    extra=4, max_num=4,
    formset=BaseQuestionChoiceFormset,
    can_delete=False)


class BaseQuestionFormset(BaseInlineFormSet):

    def add_fields(self, form, index):
        super(BaseQuestionFormset, self).add_fields(form, index)
        default_prefix = QuestionChoiceFormset.get_default_prefix()
        form.nested = QuestionChoiceFormset(
            instance=form.instance,
            data=form.data if form.is_bound else None,
            files=form.files if form.is_bound else None,
            prefix=f'choice-{form.prefix}-{default_prefix}')

    def is_valid(self):
        result = super(BaseQuestionFormset, self).is_valid()
        if self.is_bound:
            for form in self.forms:
                if hasattr(form, 'nested'):
                    result = result and form.nested.is_valid()
        return result

    def clean(self):
        super(BaseQuestionFormset, self).clean()
        form_count = len(self.forms)
        if form_count < 5:
            raise ValidationError("Викторина должна содеражать минимум 5 вопросов!")

    def save(self, commit=True):
        for form in self.forms:
            form.save(commit=commit)
        result = super(BaseQuestionFormset, self).save(commit=commit)
        for form in self.forms:
            if hasattr(form, 'nested'):
                if not self._should_delete_form(form):
                    form.nested.save(commit=commit)
        return result


QuestionFormset = inlineformset_factory(
    parent_model=Quiz,
    model=Question,
    fields='__all__',
    formset=BaseQuestionFormset,
    extra=0, min_num=1)
