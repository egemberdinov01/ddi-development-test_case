from django import forms
from ..models import Quiz


class QuizPassingForm(forms.Form):
    def __init__(self, quiz: Quiz, *args, **kwargs):
        super(QuizPassingForm, self).__init__(*args, **kwargs)

        for question in quiz.questions.all():
            label = question.question
            choices = question.choices.all()
            self.fields[question.id] = forms.ChoiceField(
                label=label,
                choices=[(choice.id, choice.choice,) for choice in choices],
                widget=forms.RadioSelect()
            )
