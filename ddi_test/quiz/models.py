from django.db import models
from user_profile.models import Profile


class Quiz(models.Model):
    title = models.CharField(verbose_name="Название викторины", max_length=300, unique=True)
    about = models.TextField(verbose_name="Описание", max_length=1000)
    author_profile = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='quiz')
    count_passes = models.IntegerField(verbose_name="Количество прохождений", default=0)
    pub_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Question(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='questions')
    question = models.CharField(verbose_name="Вопрос", max_length=1500, )

    class Meta:
        unique_together = [
            # no duplicated question per quiz
            ("question", "quiz"),
        ]


class Choice(models.Model):
    choice = models.CharField(max_length=1500, verbose_name="Текст ответа", )
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='choices')
    CHOICES = ((1, 'верный'), (0, 'неверный'))
    correct = models.IntegerField(choices=CHOICES, verbose_name="Ответ", default=0)

    class Meta:
        unique_together = [
            # no duplicated choice per question
            ("question", "choice"),
        ]


class TestResult(models.Model):
    user_profile = models.ForeignKey(Profile, verbose_name='Результаты теста', related_name='tests_results',
                                     on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    questions_count = models.IntegerField(verbose_name='Кол-во вопросов')
    correct_answer_count = models.IntegerField(verbose_name='Кол-во правильных ответов')

    def __str__(self):
        return 'Результаты теста'

    class Meta:
        unique_together = [
            ("user_profile", "quiz"),
        ]


class Comment(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='comments')
    author_profile = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='comments')
    text = models.TextField(verbose_name='')
    pub_date = models.DateTimeField(verbose_name='Дата комментария', auto_now=True)
