from django import forms
from django.contrib.auth.models import User
from .models import Profile


class LoginForm(forms.Form):
    username = forms.CharField(label='Никнейм')
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput)


class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Пароль (повторить)', widget=forms.PasswordInput)
    username = forms.CharField(label='Никнейм')
    first_name = forms.CharField(label='Имя')
    email = forms.CharField(label='email')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'email')
        help_texts = {
            'username': None,
        }

    def form_valid(self, form):
        form.save()
        return super(UserRegistrationForm, self).form_valid(form)

    def form_invalid(self, form):
        return super(UserRegistrationForm, self).form_invalid(form)


class UserEditForm(forms.ModelForm):
    class Meta:
        model = User

        fields = ('first_name', 'last_name', 'email')
        labels = {
            'first_name': 'Имя',
            'last_name': 'Фамилия',
            'email': 'email'
        }


class ProfileEditForm(forms.ModelForm):
    photo = forms.ImageField(required=False, label='Фото')

    class Meta:
        model = Profile

        fields = ('birth_date', 'about', 'photo')
        widgets = {
            'birth_date': forms.DateInput(attrs={'type': 'date'}),

        }
        labels = {
            'about': 'О себе',
            'birth_date': 'Дата рождения:',
        }
