from django.contrib import admin
from .models import Profile
from quiz.models import TestResult


class TestsResultsInline(admin.TabularInline):
    def result(self, obj):
        return f"{obj.correct_answer_count}/{obj.questions_count}"

    model = TestResult
    extra = 0
    fields = ('result', 'quiz')
    readonly_fields = ('result', 'quiz')
    can_delete = False


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    def first_name(self, obj):
        return obj.user.first_name

    def last_name(self, obj):
        return obj.user.last_name

    list_display = ('user', 'first_name', 'last_name', 'about', 'birth_date')
    fields = ('user', 'about', 'birth_date', 'first_name', 'last_name')
    readonly_fields = ('first_name', 'last_name')
    inlines = (
        TestsResultsInline,
    )
