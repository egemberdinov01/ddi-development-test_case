This is a test case for DDI Development.
========================================

About the project
-----------------
This is a simple mini-platform for creating and passing tests.
The project used Python 3.6 and Django 2.2


Convenience
--------
For convenience, the entire project was placed in docker containers running docker-compose.
Migrations, loading fixtures into the database and loading staticfiles for the admin panel will be performed automatically.


Using
-----
In order to successfully deploy a project on your machine, run the following commands:

1.  `git clone git@gitlab.com:egemberdinov01/ddi-development-test_case.git`

And then in the project folder 

2.  `docker-compose build`
3.  `docker-compose up`

By default, the project runs on your machine on port 80.

Superuser has also been created
* username: superuser
* password: superuser